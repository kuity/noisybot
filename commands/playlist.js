module.exports = {
	name: 'playlist',
	description: 'Display the current playlist',
    execute(message, args) {
        const serverQueue = message.client.queue.get(message.guild.id);
        if (!message.member.voice.channel)
            return message.channel.send(
                "You have to be in a voice channel to check the playlist!"
            );
        
        if (!serverQueue) return message.channel.send("The playlist is empty");

        songlist = serverQueue.songs;
        var titles = "```=== Current playlist === \n";
        songlist.forEach(function (song, i) {
            songindex = i+1
            titles = titles.concat(`${songindex}: ${song['title']}\n`); 
        });
        titles = titles.concat("```");
        message.channel.send(titles);
    }
}
