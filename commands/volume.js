module.exports = {
	name: 'volume',
	description: 'Adjust the volume',
    usage: '[value between 0 and 1]',
    execute(message, args) {
        const serverQueue = message.client.queue.get(message.guild.id);
        if (!message.member.voice.channel)
            return message.channel.send("You have to be in a voice channel to control the volume!");
        
        if (!serverQueue) return message.channel.send("Nothing is playing bruh");
        newVolume = args[0];
        if (newVolume<0 | newVolume>1) return message.channel.send("Volume has to be between 0 and 1");
        serverQueue.dispatcher.setVolume(newVolume);
    }
}
