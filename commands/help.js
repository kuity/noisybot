const {prefix} = require('../config.json');
module.exports = {
	name: 'help',
	description: 'List all comannds',
	execute(message, args) {
        const {commands} = message.client;
        var helpstring = "```=== Commands === \n";
        commands.forEach(command => {
            if (command.usage) helpstring = helpstring.concat(
                `${prefix}${command.name} ${command.usage}`.padEnd(40) 
                + `${command.description}\n`);
            else helpstring = helpstring.concat(`${prefix}${command.name}`.padEnd(40) 
                + `${command.description}\n`);
        });

        helpstring = helpstring.concat("```");
        message.channel.send(helpstring);
	}
};
